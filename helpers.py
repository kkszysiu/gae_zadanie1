import re

from google.appengine.api import urlfetch


class SiteMgr(object):
    def __init__(self, word):
        self.cnt = 0
        self.rpcs = []
        self.word = word
        self._sites = ['http://wp.pl/',
                       'http://onet.pl/',
                       'http://interia.pl/',
                       'http://o2.pl/']

    def get_cnt(self, body):
        return len([m.start() for m in re.finditer(self.word, body)])

    def handle_result(self, rpc):
        result = rpc.get_result()

        if result.status_code == 200:
            body = result.content
            #print "word", self.word
            #print "result", result

            #print "get_cnt(word, body)", self.get_cnt(body)
            self.cnt += self.get_cnt(body)

    # Use a helper function to define the scope of the callback.
    def create_callback(self, rpc):
        return lambda: self.handle_result(rpc)

    def count_words(self):
        for url in self._sites:
            rpc = urlfetch.create_rpc()
            rpc.callback = self.create_callback(rpc)
            urlfetch.make_fetch_call(rpc, url)
            self.rpcs.append(rpc)

        # Finish all RPCs, and let callbacks process the results.
        for rpc in self.rpcs:
            rpc.wait()

        return self.cnt
