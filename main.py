import webapp2
import random
from webapp2_extras import jinja2

from google.appengine.ext import ndb
from google.appengine.api import taskqueue

from models import Task
from helpers import SiteMgr


class JinjaRequestHandler(webapp2.RequestHandler):
    @webapp2.cached_property
    def jinja2(self):
        return jinja2.get_jinja2(app=self.app)

    def render_response(self, template, **context):
        rendered_value = self.jinja2.render_template(template, **context)
        self.response.write(rendered_value)


class ResultHandler(JinjaRequestHandler):
    def get(self, task_id):
        ndb_key = ndb.Key(Task, str(task_id))
        taskobj = ndb_key.get()

        self.render_response('result.html', taskobj=taskobj)


class SearchHandler(JinjaRequestHandler):
    def get(self):
        self.render_response('search.html')

    def post(self):
        searchphrase = self.request.get('search_phrase')
        task = Task(id=searchphrase, word=searchphrase, count=None)
        k = task.put()

        taskqueue.add(url='/worker', params={'id': k.id()})

        redirectUrl = "/result/%s/" % k.id()
        self.redirect(redirectUrl)


class ListHandler(JinjaRequestHandler):
    def get(self):
        taskobjs = Task.query().order(-Task.count)
        #taskobjs = []
        self.render_response('list.html', taskobjs=taskobjs)


class HomeHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write("<a href='/search'>Search</a>")


class TaskWorker(webapp2.RequestHandler):
    def post(self):
        id = self.request.get('id')
        #print "TaskWorker", id
        ndb_key = ndb.Key(Task, str(id))
        taskobj = ndb_key.get()

        if hasattr(taskobj, 'word'):
            searched_word = taskobj.word
            sitemgr = SiteMgr(searched_word)

            wordscnt = sitemgr.count_words()
            #print "wordscnt", wordscnt
            taskobj.count = wordscnt

            taskobj.put()


class TaskRecountWorker(webapp2.RequestHandler):
    def get(self):
        taskkeys = Task.query().order(-Task.count).fetch(keys_only=True)

        for taskkey in taskkeys:
            taskqueue.add(url='/worker', params={'id': taskkey.id()})


APPLICATION = webapp2.WSGIApplication([('/', HomeHandler),
                                       ('/list', ListHandler),
                                       ('/search', SearchHandler),
                                       ('/result/(\w+)/', ResultHandler),
                                       ('/result/(\w+)', ResultHandler),
                                       ('/worker', TaskWorker),
                                       #('/task/recount', TaskRecountWorker)
                                       ],
                                      debug=True)

CRONAPP = webapp2.WSGIApplication([
                                       ('/task/recount', TaskRecountWorker)
                                       ],
                                      debug=True)