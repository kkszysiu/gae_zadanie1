from google.appengine.ext import ndb


class Task(ndb.Model):
    word = ndb.StringProperty()
    count = ndb.IntegerProperty(default=0)
